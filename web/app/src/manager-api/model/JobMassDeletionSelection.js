/**
 * Flamenco manager
 * Render Farm manager API
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The JobMassDeletionSelection model module.
 * @module model/JobMassDeletionSelection
 * @version 0.0.0
 */
class JobMassDeletionSelection {
    /**
     * Constructs a new <code>JobMassDeletionSelection</code>.
     * Parameters to describe which jobs should be deleted.
     * @alias module:model/JobMassDeletionSelection
     */
    constructor() { 
        
        JobMassDeletionSelection.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>JobMassDeletionSelection</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/JobMassDeletionSelection} obj Optional instance to populate.
     * @return {module:model/JobMassDeletionSelection} The populated <code>JobMassDeletionSelection</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new JobMassDeletionSelection();

            if (data.hasOwnProperty('last_updated_max')) {
                obj['last_updated_max'] = ApiClient.convertToType(data['last_updated_max'], 'Date');
            }
        }
        return obj;
    }


}

/**
 * All jobs that were last updated before or on this timestamp will be deleted.
 * @member {Date} last_updated_max
 */
JobMassDeletionSelection.prototype['last_updated_max'] = undefined;






export default JobMassDeletionSelection;

