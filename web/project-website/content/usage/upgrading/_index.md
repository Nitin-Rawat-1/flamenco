---
title: Upgrading Flamenco
---

1. To ensure your current jobs are rendered consistently, make sure to finish them before proceeding.
2. Download the [latest version](https://flamenco.blender.org/download/) of Flamenco.
3. Stop the Flamenco Manager and Workers on all computers.
4. Extract the downloaded ZIP in the same location as the previously installed Flamenco Manager, overwriting the existing files.
5. Run the Flamenco Manager executable.
    1. Open the web app in your browser.
    2. Inside the Manager, in the top-right corner, download the updated Blender add-on.
6. In Blender, reinstall the add-on. After installing it, you should see the version change.
    1. Inside the add-on panel, check your preferences and press the refresh button on the Manager URL field. 
7. Replace the Workers executables on each computer with the newer version.
    1. Run them again, they should sign in to the Manager as usual.
8. Send your jobs to your updated render farm.